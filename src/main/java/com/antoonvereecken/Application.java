package com.antoonvereecken;

import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.http.javadsl.Http;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.*;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import com.antoonvereecken.model.Quote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CompletionStage;

public class Application {
    ActorSystem<?> actorSystem;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public void run(String symbol) {
        actorSystem = ActorSystem.create(Behaviors.empty(), "actorSystem");
        Unmarshaller<HttpEntity, Quote> quoteUnmarshaller = Jackson.unmarshaller(Quote.class);

        CompletionStage<HttpResponse> futureResponse = Http.get(actorSystem)
                .singleRequest(requestQuoteBySymbol(symbol));

        futureResponse.whenComplete((httpResponse, throwable) -> {
            if (throwable != null) logger.error("Something went wrong: %d", throwable);
            else {
                CompletionStage<Quote> unmarshalCS = quoteUnmarshaller.unmarshal(httpResponse.entity(), actorSystem);
                unmarshalCS.whenComplete((quote, throwable1) -> {
                    quote.setSymbol(symbol);
                    logger.debug(quote.toString());
                    logger.debug("quote.getTimestamp().toString() = {}", quote.getTimestamp());
                });
            }
            httpResponse.discardEntityBytes(actorSystem);
        });

    }

    private HttpRequest requestQuoteBySymbol(String symbol) {
        logger.debug("Constructing HttpRequest to retrieve quote for symbol: {}", symbol);
        List<HttpHeader> headers = List.of(
            RawHeader.create("X-Finnhub-Token", "c1rddr2ad3iautolma5g")
        );
        return HttpRequest.create()
            .withMethod(HttpMethods.GET)
            .withUri("https://finnhub.io/api/v1/quote?symbol="+symbol)
            .withHeaders(headers);
    }
}
