package com.antoonvereecken.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;
import java.util.concurrent.atomic.AtomicInteger;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Quote {
    private static final AtomicInteger count = new AtomicInteger(0);
    private final int id;
    private String symbol;
    private final double openPriceOfTheDay;     // o
    private final double highPriceOfTheDay;     // h
    private final double lowPriceOfTheDay;      // l
    private final double currentPrice;          // c
    private final double previousClosedPrice;   // pc
    private final long timestamp;               // t

    @JsonCreator
    public Quote(
        @JsonProperty(value = "o") double openPriceOfTheDay,
        @JsonProperty(value = "h") double highPriceOfTheDay,
        @JsonProperty(value = "l") double lowPriceOfTheDay,
        @JsonProperty(value = "c") double currentPrice,
        @JsonProperty(value = "pc") double previousClosedPrice,
        @JsonProperty(value = "t") long timestamp
        ) {
        this.id = count.incrementAndGet();
        this.openPriceOfTheDay = openPriceOfTheDay;
        this.highPriceOfTheDay = highPriceOfTheDay;
        this.lowPriceOfTheDay = lowPriceOfTheDay;
        this.currentPrice = currentPrice;
        this.previousClosedPrice = previousClosedPrice;
        this.timestamp = System.currentTimeMillis();
    }

    public int getId() { return id; }
    public double getOpenPriceOfTheDay() { return openPriceOfTheDay; }
    public double getHighPriceOfTheDay() { return highPriceOfTheDay; }
    public double getLowPriceOfTheDay() { return lowPriceOfTheDay; }
    public double getCurrentPrice() { return currentPrice; }
    public double getPreviousClosedPrice() { return previousClosedPrice; }
    public Timestamp getTimestamp() { return new Timestamp(timestamp); }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "Quote{" +
            "id=" + id +
            ", symbol=" + symbol +
            ", currentPrice=" + currentPrice +
            ", previousClosedPrice=" + previousClosedPrice +
            ", openPriceOfTheDay=" + openPriceOfTheDay +
            ", lowPriceOfTheDay=" + lowPriceOfTheDay +
            ", highPriceOfTheDay=" + highPriceOfTheDay +
            ", timestamp=" + timestamp +
            '}';
    }
}
